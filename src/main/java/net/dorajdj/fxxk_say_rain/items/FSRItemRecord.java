package net.dorajdj.fxxk_say_rain.items;

import net.dorajdj.fxxk_say_rain.core.FSRRef;
import net.minecraft.item.ItemRecord;
import net.minecraft.util.ResourceLocation;

public class FSRItemRecord extends ItemRecord
{
	public FSRItemRecord(String name)
	{
		super(name);
	}
	
	@Override
	public ResourceLocation getRecordResource(String name)
	{
		return new ResourceLocation(FSRRef.FSRModid + ":records." + name);
	}
}
