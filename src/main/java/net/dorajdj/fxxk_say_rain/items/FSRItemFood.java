package net.dorajdj.fxxk_say_rain.items;

import cpw.mods.fml.common.registry.GameRegistry;
import net.dorajdj.fxxk_say_rain.Fxxk_say_rain;
import net.dorajdj.fxxk_say_rain.core.FSRRef;
import net.minecraft.item.ItemFood;

public class FSRItemFood extends ItemFood
{
	String name;
    
	public FSRItemFood(String name, int hunger, float saturation, boolean canwolfeat)
	{
		super(hunger, saturation, canwolfeat);
		this.setCreativeTab(Fxxk_say_rain.tabFSR);
		this.setUnlocalizedName(name);
		this.name = name;
		this.setTextureName(FSRRef.FSRModid + ":" + name);
		GameRegistry.registerItem(this, name);
	}
}
