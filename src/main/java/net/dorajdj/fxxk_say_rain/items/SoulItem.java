package net.dorajdj.fxxk_say_rain.items;

import java.util.List;

import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class SoulItem extends Item
{
    public SoulItem()
    {
        this.setMaxStackSize(64);
    }
    
    @Override
    public void addInformation(ItemStack itemStack, EntityPlayer entityPlayer, List list, boolean b)
    {
        list.add(I18n.format("item.soulItem.desc", new Object[0]));
    }
}
