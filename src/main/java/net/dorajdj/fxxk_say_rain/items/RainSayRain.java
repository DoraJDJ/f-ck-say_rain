package net.dorajdj.fxxk_say_rain.items;

import java.util.List;
import java.util.Random;

import net.dorajdj.fxxk_say_rain.core.FSRConfigHandler;
import net.dorajdj.fxxk_say_rain.core.FSRMsgUtils;
import net.dorajdj.fxxk_say_rain.core.FSRRef;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.monster.EntitySilverfish;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.entity.passive.EntityCow;
import net.minecraft.entity.passive.EntityMooshroom;
import net.minecraft.entity.passive.EntityPig;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;
import net.minecraft.world.storage.WorldInfo;

public class RainSayRain extends FSRItemFood
{
    public RainSayRain(String name, int var2, float var3, boolean var4)
    {
        super(name, var2, var3, var4);
        this.setPotionEffect(Potion.blindness.id, 5, 1, 1.0F);
    }
    
    @Override
    public void addInformation(ItemStack itemStack, EntityPlayer entityPlayer, List list, boolean b)
    {
        list.add(I18n.format("item.sayRainFood.desc", new Object[0]));
        list.add(I18n.format("item.rainSayRain.desc", new Object[0]));
    }
    
    @Override
    public EnumRarity getRarity(ItemStack itemstack)
    {
    	return EnumRarity.epic;
    }
    
    public ItemStack onEaten(ItemStack itemstack, World world, EntityPlayer player)
    {
    	super.onEaten(itemstack, world, player);
        
    	if (!world.isRemote)
    	{
    		world.playSoundAtEntity(player, FSRRef.FSRModid + ":poi", 1.0F, 1.0F);
    		
    		if (FSRConfigHandler.doSomeStrange = true)
    		{
        		int number = new Random().nextInt(5) + 1;
        		
        		double playerX = player.posX;
        		double playerY = player.posY;
        		double playerZ = player.posZ;
        		
        		if (number == 1)
        		{
        			EntityPig londingPig = new EntityPig(world);
        			londingPig.setCustomNameTag("Londing");
        			londingPig.setPosition(playerX, playerY, playerZ);
        			world.spawnEntityInWorld(londingPig);
        			FSRMsgUtils.addMessage(player, "§6野生的读条猪出现啦！");
        		}
        		else if (number == 2)
                {
        			EntityCow bakaCow = new EntityCow(world);
        			bakaCow.setCustomNameTag("Baka_L");
        			bakaCow.setPosition(playerX, playerY, playerZ);
        			world.spawnEntityInWorld(bakaCow);
        			FSRMsgUtils.addMessage(player, "§6野生的波卡牛出现啦！");
                }
        		else if (number == 3)
                {
                	EntityChicken sayRainChicken = new EntityChicken(world);
                	sayRainChicken.setCustomNameTag("Say_rain");
                	sayRainChicken.setPosition(playerX, playerY, playerZ);
                	world.spawnEntityInWorld(sayRainChicken);
                	FSRMsgUtils.addMessage(player, "§6野生的塞卵鸡出现啦！");
                }
        		else if (number == 4)
                {
        			// 备注：这么长的代号，我要爆炸啦！
                	EntityMooshroom gundamFreedomMooshroom = new EntityMooshroom(world);
                	gundamFreedomMooshroom.setCustomNameTag("GundamFreedom");
                	gundamFreedomMooshroom.setPosition(playerX, playerY, playerZ);
                	world.spawnEntityInWorld(gundamFreedomMooshroom);
                	FSRMsgUtils.addMessage(player, "§6野生的大爷蘑菇牛出现啦！");
                }
        		else if (number == 5)
                {
                	EntitySilverfish funFish = new EntitySilverfish(world);
                	
                	int randomPos = new Random().nextInt(6) + 1;
                	
                	funFish.setCustomNameTag("s_fun");
                	funFish.setPosition(playerX + randomPos, playerY, playerZ + randomPos);
                	world.spawnEntityInWorld(funFish);
                	FSRMsgUtils.addMessage(player, "§6野生的蠹虫fun出现啦！");
                	FSRMsgUtils.addChatMessage(player, "s_fun", "我只看到鸡蛋卷和朴莉的头，顶着各自的[哔——]");
                }
        		else
                {
                	FSRMsgUtils.addMessage(player, "§6什么事都没有发生OAO！");
                }
    		}
    	}
        
        return itemstack;
    }
}
