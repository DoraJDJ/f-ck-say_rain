package net.dorajdj.fxxk_say_rain.items;

import java.util.List;
import java.util.Random;

import net.dorajdj.fxxk_say_rain.core.FSRMsgUtils;
import net.dorajdj.fxxk_say_rain.core.FSRRef;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;

public class SayRainBread extends FSRItemFood
{
    public SayRainBread(String name, int var2, float var3, boolean var4)
	{
    	super(name, var2, var3, var4);
    }
    
    @Override
    public void addInformation(ItemStack itemStack, EntityPlayer entityPlayer, List list, boolean b)
    {
        list.add(I18n.format("item.sayRainFood.desc", new Object[0]));
    }
    
    public ItemStack onEaten(ItemStack itemstack, World world, EntityPlayer player)
    {
    	super.onEaten(itemstack, world, player);
    	
    	if (!world.isRemote)
    	{
    	    world.playSoundAtEntity(player, FSRRef.FSRModid + ":poi", 1.0F, 1.0F);
    	    
    		int number = new Random().nextInt(7) + 1;
	        //System.out.println(number);

            if (number == 1)
            {
            	/* 塞卵就是岛风 */
            	FSRMsgUtils.addTellMessage(player, "Say_rain", "我是驱逐舰岛风。速度的话不会输给任何人的。疾如岛风，de-su！");
            }
            else if (number == 2)
            {
            	FSRMsgUtils.addTellMessage(player, "baka_cirno_9", "再这样poipoi的话，我就用我的附卡把你poi掉哦~");
            }
            else if (number == 3)
            {
            	FSRMsgUtils.addTellMessage(player, "baka66", "大绿兔最萌，塞卵是辣鸡OAO你说是吧？");
            }
            else if (number == 4)
            {
            	FSRMsgUtils.addTellMessage(player, "Baka_L", "白兔正教千载万代，你也来加入吧！");
            }
            else if (number == 5)
            {
				FSRMsgUtils.addTellMessage(player, "s_fun", "肥羊大法好");
            }
            else if (number == 6)
            {
                FSRMsgUtils.addTellMessage(player, "Londing", "肛，老娘不是RBQ！");
            }
            else if (number == 7)
            {
                FSRMsgUtils.addTellMessage(player, "Londing", "我是男的！");
            }
            else if (number == 8)
            {
                /* 因为随机数值只能随机到7，所以这个信息并不会正常显示，除非你把源代码改了 */
                FSRMsgUtils.addTellMessage(player, "bolilingmeng", "傻逼玩意，说话不会好好说对吧，你是惹毛我了告诉你，这事没完");
            }
            else if (number == 450)
            {
                /* 因为随机数值最多只能随机到7，所以这个信息并不会正常显示，除非你把源代码给改了 */
            	FSRMsgUtils.addTellMessage(player, "Zhehe", "嘿伙计，我诈尸啦");
            }
            else
            {
            	/* 塞卵就是岛风 */
            	FSRMsgUtils.addTellMessage(player, "Say_rain", "提督，我的奔跑你看到了吗？很快吧？很快吧？呼呼呼~");
            }
    	}
    	
    	return itemstack;
    }
}
