package net.dorajdj.fxxk_say_rain.items;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;

public class SoulWww extends SoulItem
{
	public final String soulId;
	
	public SoulWww(String soulIdd)
	{
		this.soulId = soulIdd;
	}
	
	@Override
    public void addInformation(ItemStack itemStack, EntityPlayer entityPlayer, List list, boolean b)
    {
    	Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        
        if (calendar.get(2) + 1 == 4 && calendar.get(5) == 1)
        {
            // HAPPY APRIL FOOLS!
        	list.add(I18n.format("item." + this.soulId + "Www.desc1", new Object[0]));
        	list.add(I18n.format("item." + this.soulId + "Www.desc2", new Object[0]));
        	list.add(I18n.format("item.soulWww.desc", new Object[0]));
        }
        else
        {
            list.add(I18n.format("item.soulItem.desc", new Object[0]));
        }
    }
    
    @Override
    public EnumRarity getRarity(ItemStack itemstack)
    {
    	Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
    	
    	if (calendar.get(2) + 1 == 4 && calendar.get(5) == 1)
    	{
    		return EnumRarity.epic;
    	}
    	
    	return EnumRarity.common;
    }
}
