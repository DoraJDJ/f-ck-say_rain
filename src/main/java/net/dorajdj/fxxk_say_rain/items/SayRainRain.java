package net.dorajdj.fxxk_say_rain.items;

import java.util.List;

import net.dorajdj.fxxk_say_rain.core.FSRRef;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;
import net.minecraft.world.storage.WorldInfo;

public class SayRainRain extends FSRItemFood
{
    public SayRainRain(String name, int var2, float var3, boolean var4)
    {
        super(name, var2, var3, var4);
        this.setPotionEffect(Potion.moveSpeed.id, 30, 3, 1.0F);
    }
    
    @Override
    public void addInformation(ItemStack itemStack, EntityPlayer entityPlayer, List list, boolean b)
    {
        list.add(I18n.format("item.sayRainFood.desc", new Object[0]));
    }
    
    public ItemStack onEaten(ItemStack itemstack, World world, EntityPlayer player)
    {
    	super.onEaten(itemstack, world, player);
        
    	if (!world.isRemote)
    	{
    		world.playSoundAtEntity(player, FSRRef.FSRModid + ":poi", 1.0F, 1.0F);
    	}
        
        return itemstack;
    }
}
