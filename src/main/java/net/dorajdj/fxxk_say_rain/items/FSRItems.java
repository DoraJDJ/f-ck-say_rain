package net.dorajdj.fxxk_say_rain.items;

import java.util.Calendar;
import java.util.Date;

import net.dorajdj.fxxk_say_rain.Fxxk_say_rain;
import net.dorajdj.fxxk_say_rain.core.FSRConfigHandler;
import net.dorajdj.fxxk_say_rain.core.FSRLogManager;
import net.dorajdj.fxxk_say_rain.core.FSRRef;
import net.dorajdj.fxxk_say_rain.core.FSRUtils;
import net.minecraft.item.Item;

public class FSRItems extends Item
{
    public static Item soulSeed;
    public static Item sayRainSoul;
    public static Item bakaLSoul;
    public static Item londingSoul;
    public static Item doraJdjSoul;
    public static Item puliSoul;
    public static Item raikoSoul;
    public static Item himekoSoul;
    
    public static Item yamamotoZn;
    
    public static Item recordTest;
    
    public static Item sayRainCookie;
    public static Item sayRainBread;
    public static Item sayRainRain;
    public static Item rainSayRain;
    
    public static void registerItems()
    {
    	Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        
        if (calendar.get(2) + 1 == 4 && calendar.get(5) == 1)
        {
            // HAPPY APRIL FOOLS!
        	FSRItems.sayRainSoul = new SoulWww("sayRain").setTextureName(FSRRef.FSRModid + ":" + FSRRef.sayRainWwwID).setCreativeTab(Fxxk_say_rain.tabFSR).setUnlocalizedName(FSRRef.soulWwwID);
            FSRItems.londingSoul = new SoulWww("londing").setTextureName(FSRRef.FSRModid + ":" + FSRRef.londingWwwID).setCreativeTab(Fxxk_say_rain.tabFSR).setUnlocalizedName(FSRRef.soulWwwID);
            FSRItems.himekoSoul = new SoulWww("himeko").setTextureName(FSRRef.FSRModid + ":" + FSRRef.himekoWwwID).setCreativeTab(Fxxk_say_rain.tabFSR).setUnlocalizedName(FSRRef.soulWwwID);
        }
        else
        {
            FSRItems.sayRainSoul = new SoulWww("sayRain").setTextureName(FSRRef.FSRModid + ":" + FSRRef.sayRainSoulID).setCreativeTab(Fxxk_say_rain.tabFSR).setUnlocalizedName(FSRRef.sayRainSoulID);
            FSRItems.londingSoul = new SoulWww("londing").setTextureName(FSRRef.FSRModid + ":" + FSRRef.londingSoulID).setCreativeTab(Fxxk_say_rain.tabFSR).setUnlocalizedName(FSRRef.londingSoulID);
            FSRItems.himekoSoul = new SoulWww("himeko").setTextureName(FSRRef.FSRModid + ":" + FSRRef.himekoSoulID).setCreativeTab(Fxxk_say_rain.tabFSR).setUnlocalizedName(FSRRef.himekoSoulID);
        }

        FSRItems.soulSeed = new SoulSeed().setTextureName(FSRRef.FSRModid + ":" + FSRRef.soulSeedID).setCreativeTab(Fxxk_say_rain.tabFSR).setUnlocalizedName(FSRRef.soulSeedID);
        FSRItems.bakaLSoul = new SoulItem().setTextureName(FSRRef.FSRModid + ":" + FSRRef.bakaLSoulID).setCreativeTab(Fxxk_say_rain.tabFSR).setUnlocalizedName(FSRRef.bakaLSoulID);
        FSRItems.doraJdjSoul = new SoulItem().setTextureName(FSRRef.FSRModid + ":" + FSRRef.doraJdjSoulID).setCreativeTab(Fxxk_say_rain.tabFSR).setUnlocalizedName(FSRRef.doraJdjSoulID);
        FSRItems.puliSoul = new SoulItem().setTextureName(FSRRef.FSRModid + ":" + FSRRef.puliSoulID).setCreativeTab(Fxxk_say_rain.tabFSR).setUnlocalizedName(FSRRef.puliSoulID);
        FSRItems.raikoSoul = new SoulItem().setTextureName(FSRRef.FSRModid + ":" + FSRRef.raikoSoulID).setCreativeTab(Fxxk_say_rain.tabFSR).setUnlocalizedName(FSRRef.raikoSoulID);
        
        FSRItems.yamamotoZn = new YamamotoZn().setTextureName(FSRRef.FSRModid + ":" + FSRRef.yamamotoZnID).setCreativeTab(Fxxk_say_rain.tabFSR).setUnlocalizedName(FSRRef.yamamotoZnID);
        
        // RECORD TESTING!
        FSRItems.recordTest = new FSRItemRecord("recordTest").setTextureName(FSRRef.FSRModid + ":recordTest").setCreativeTab(Fxxk_say_rain.tabFSR).setUnlocalizedName("recordTest");

        FSRUtils.registerItem(true, FSRItems.soulSeed, FSRRef.soulSeedID);
        FSRUtils.registerItem(true, FSRItems.sayRainSoul, FSRRef.sayRainSoulID);
        FSRUtils.registerItem(true, FSRItems.bakaLSoul, FSRRef.bakaLSoulID);
        FSRUtils.registerItem(true, FSRItems.londingSoul, FSRRef.londingSoulID);
        FSRUtils.registerItem(FSRConfigHandler.notFFF, FSRItems.doraJdjSoul, FSRRef.doraJdjSoulID);
        FSRUtils.registerItem(FSRConfigHandler.notFFF, FSRItems.puliSoul, FSRRef.puliSoulID);
        FSRUtils.registerItem(true, FSRItems.raikoSoul, FSRRef.raikoSoulID);
        FSRUtils.registerItem(true, FSRItems.himekoSoul, FSRRef.himekoSoulID);
        
        FSRUtils.registerItem(FSRConfigHandler.enableYamamotoZinc, FSRItems.yamamotoZn, FSRRef.yamamotoZnID);
        FSRUtils.registerOre(FSRConfigHandler.enableYamamotoZinc, "ingotZinc", FSRItems.yamamotoZn);
        
        //FSRUtils.registerItem(true, recordTest, "recordTest");
        
        FSRItems.sayRainCookie = new SayRainCookie("sayRainCookie", 2, 0.1F, false);
        FSRItems.sayRainBread = new SayRainBread("sayRainBread", 5, 0.6F, false);
        FSRItems.sayRainRain = new SayRainRain("sayRainRain", 6, 0.6F, false);
        FSRItems.rainSayRain = new RainSayRain("rainSayRain", 6, 0.6F, false);
        
        FSRLogManager.log("Register Items complete");
    }
}
