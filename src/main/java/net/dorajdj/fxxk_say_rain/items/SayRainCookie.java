package net.dorajdj.fxxk_say_rain.items;

import java.util.List;
import java.util.Random;

import net.dorajdj.fxxk_say_rain.core.FSRMsgUtils;
import net.dorajdj.fxxk_say_rain.core.FSRRef;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;

public class SayRainCookie extends FSRItemFood
{
    public SayRainCookie(String name, int var2, float var3, boolean var4)
	{
    	super(name, var2, var3, var4);
    }
    
    @Override
    public void addInformation(ItemStack itemStack, EntityPlayer entityPlayer, List list, boolean b)
    {
        list.add(I18n.format("item.sayRainFood.desc", new Object[0]));
    }
    
    public ItemStack onEaten(ItemStack itemstack, World world, EntityPlayer player)
    {
    	super.onEaten(itemstack, world, player);
    	
    	if (!world.isRemote)
    	{
    	    world.playSoundAtEntity(player, FSRRef.FSRModid + ":poi", 1.0F, 1.0F);
    	    
    		int number = new Random().nextInt(10) + 1;
	        //System.out.println(number);

            if (number == 1)
            {
                FSRMsgUtils.addJoinMessage(player, "Say_rain");
            }
            else if (number == 2)
            {
				FSRMsgUtils.addJoinMessage(player, "baka_cirno_9");
            }
            else if (number == 3)
            {
				FSRMsgUtils.addJoinMessage(player, "baka66");
            }
            else if (number == 4)
            {
				FSRMsgUtils.addJoinMessage(player, "Baka_L");
            }
            else if (number == 5)
            {
				FSRMsgUtils.addJoinMessage(player, "kenrou_holo");
            }
            else if (number == 6)
            {
				FSRMsgUtils.addJoinMessage(player, "zeratulo0");
            }
            else if (number == 7)
            {
				FSRMsgUtils.addJoinMessage(player, "Aegis");
            }
            else if (number == 8)
            {
				FSRMsgUtils.addJoinMessage(player, "jinyin");
            }
            else if (number == 9)
            {
				FSRMsgUtils.addJoinMessage(player, "DFzhizi");
            }
            else if (number == 10)
            {
				FSRMsgUtils.addJoinMessage(player, "Emp");
            }
            else if (number == 450)
            {
                /* 因为随机数值最多只能随机到10，所以这个信息并不会正常显示，除非你把源代码给改了 */
				FSRMsgUtils.addJoinMessage(player, "Zhehe");
            }
            else
            {
				FSRMsgUtils.addJoinMessage(player, "Say_rain");
            }
    	}
    	
    	return itemstack;
    }
}
