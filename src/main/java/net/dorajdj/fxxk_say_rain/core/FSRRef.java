package net.dorajdj.fxxk_say_rain.core;

public class FSRRef
{
    public static final String FSRModid = "fxxksayrain";
    public static final String FSRModname = "F@ck Say_rain";
    public static final String FSRVer = "15.04";
    
    /* Creative Tab */
    public static final String FSRTab = "tabFSR";
    
    /* Blocks */
    public static final String sayRainBlockID = "sayRainBlock";
    public static final String shinySayRainBlockID = "shinySayRainBlock";
    public static final String bakaLBlockID = "bakaLBlock";
    public static final String shinyBakaLBlockID = "shinyBakaLBlock";
    public static final String londingBlockID = "londingBlock";
    public static final String shinyLondingBlockID = "shinyLondingBlock";
    public static final String doraJdjBlockID = "doraJdjBlock";
    public static final String shinyDoraJdjBlockID = "shinyDoraJdjBlock";
    public static final String puliBlockID = "puliBlock";
    public static final String shinyPuliBlockID = "shinyPuliBlock";
    public static final String raikoBlockID = "raikoBlock";
    public static final String shinyRaikoBlockID = "shinyRaikoBlock";
    public static final String himekoBlockID = "himekoBlock";
    public static final String shinyHimekoBlockID = "shinyHimekoBlock";
    
    public static final String yamaZnBlockID = "yamaZnBlock";
    
    /* Items */
    public static final String soulSeedID = "soulSeed";
    public static final String soulWwwID = "soulWww";
    public static final String sayRainSoulID = "sayRainSoul";
    public static final String sayRainWwwID = "sayRainWww";
    public static final String bakaLSoulID = "bakaLSoul";
    public static final String londingSoulID = "londingSoul";
    public static final String londingWwwID = "londingWww";
    public static final String doraJdjSoulID = "doraJdjSoul";
    public static final String puliSoulID = "puliSoul";
    public static final String raikoSoulID = "raikoSoul";
    public static final String himekoSoulID = "himekoSoul";
    public static final String himekoWwwID = "himekoWww";
    
    public static final String yamamotoZnID = "yamamotoZn";
    
    /* Configuration */
    public static final String configTypeCrafting = "CraftingSettings";
    public static final String configTypeBlock = "BlockSettings";
    public static final String configTypeReg = "RegistrySettings";
    public static final String configTypeMisc = "MiscSettings";
    public static final String configTypeBlockMsg = "MessageSettings";
}
