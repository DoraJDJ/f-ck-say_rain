package net.dorajdj.fxxk_say_rain.core;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FSRLogManager
{
    public static final Logger FSRLogger = LogManager.getLogger(FSRRef.FSRModid);
    
    public static void deBugLog(String message)
    {
        FSRLogger.debug(message);
    }
    
    public static void error(String message)
    {
        FSRLogger.error(message);
    }
    
    public static void log(String message)
    {
        FSRLogger.info(message);
    }
    
    public static void log(Level level, String message)
    {
        FSRLogger.log(level, message);
    }
    
    public static void warn(String message)
    {
        FSRLogger.warn(message);
    }
}
