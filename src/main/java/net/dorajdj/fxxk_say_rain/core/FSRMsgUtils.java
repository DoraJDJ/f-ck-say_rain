package net.dorajdj.fxxk_say_rain.core;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;

/**
 * @author DoraJDJ
 */
public class FSRMsgUtils
{
	/**
	 * 单纯地向玩家发送信息罢了
	 * 
	 * @param player 指定玩家，在源代码内通常指EntityPlayer
	 * @param message 信息内容
	 */
	public static void addMessage(EntityPlayer player, String message)
	{
		player.addChatComponentMessage(new ChatComponentText(message));
	}
	
	/**
	 * 向玩家发送伪·聊天信息（格式参考Minecraft原版）
	 * 
	 * @param player 指定玩家，在源代码内通常指EntityPlayer
	 * @param username 指定伪造聊天信息的发送者
	 * @param message 指定伪造聊天信息的内容
	 */
	public static void addChatMessage(EntityPlayer player, String username, String message)
	{
		player.addChatComponentMessage(new ChatComponentText("<" + username + "> " + message));
	}
	
	/**
	 * 向玩家发送伪·聊天信息（格式参考CirnoCraft）<br \><br \>
	 * 需要注意的是，此方法仅为参考，并不会正式使用在该模组里。
	 * 
	 * @param player 指定玩家，在源代码内通常指EntityPlayer
	 * @param prefix 指定信息发送者的前缀（非特殊前缀的情况下，前缀的格式是这样的：“§e[§b<前缀>§e]”）
	 * @param username 指定伪造聊天信息的发送者
	 * @param suffix 指定信息发送者的后缀（如果没有后缀，请直接""留空）
	 * @param message 指定伪造聊天信息的内容
	 */
	public static void addChatMessageCc(EntityPlayer player, String prefix, String username, String suffix, String message)
	{
		player.addChatComponentMessage(new ChatComponentText("[world]" + prefix + "§r" + username + "§r" + suffix + "§r" + ": " + message));
	}
	
	/**
	 * 向玩家发送伪·聊天信息（格式参考CirnoCraft）<br \>
	 * 如果你想弄出个来自异世界的玩家发出的信息，请使用此方法<br \><br \>
	 * 需要注意的是，此方法仅为参考，并不会正式使用在该模组里。
	 * 
	 * @param player 指定玩家，在源代码内通常指EntityPlayer
	 * @param worldPrefix 指定信息发送者所在的世界
	 * @param prefix 指定信息发送者的前缀（非特殊前缀的情况下，前缀的格式是这样的：“§e[§b<前缀>§e]”）
	 * @param username 指定伪造聊天信息的发送者
	 * @param suffix 指定信息发送者的后缀（如果没有后缀，请直接""留空）
	 * @param message 指定伪造聊天信息的内容
	 */
	public static void addChatMessageCc(EntityPlayer player, String worldPrefix, String prefix, String username, String suffix, String message)
	{
		player.addChatComponentMessage(new ChatComponentText("[" + worldPrefix + "]" + prefix + "§r" + username + "§r" + suffix + "§r" + ": " + message));
	}
	
	/**
	 * 向玩家发送伪·悄悄话信息（格式参考Minecraft原版）
	 * 
	 * @param player 指定玩家，在源代码内通常指EntityPlayer
	 * @param username 指定伪造悄悄话信息的发送者
	 * @param message 指定伪造悄悄话信息的内容
	 */
	public static void addTellMessage(EntityPlayer player, String username, String message)
	{
		player.addChatComponentMessage(new ChatComponentText("§7§o" + username + "悄悄地对你说：" + message));
	}
	
	/**
	 * 向玩家发送伪·悄悄话信息（格式参考CirnoCraft）<br \><br \>
	 * 需要注意的是，此方法仅为参考，并不会正式使用在该模组里。
	 * 
	 * @param player 指定玩家，在源代码内通常指EntityPlayer
	 * @param prefix 指定信息发送者的前缀（非特殊前缀的情况下，前缀的格式是这样的：“§e[§b<前缀>§e]”）
	 * @param username 指定伪造悄悄话信息的发送者
	 * @param suffix 指定信息发送者的后缀（如果没有后缀，请直接""留空）
	 * @param message 指定伪造悄悄话信息的内容
	 */
	public static void addTellMessageCc(EntityPlayer player, String prefix, String username, String suffix, String message)
	{
		player.addChatComponentMessage(new ChatComponentText("§6[" + prefix + "§r" + username + "§r" + suffix + "§r§6 -> 我] §r" + message));
	}
	
	/**
	 * 向玩家发送伪·玩家加入信息（格式参考Minecraft原版）
	 * 
	 * @param player 指定玩家，在源代码内通常指EntityPlayer
	 * @param username 指定伪造玩家加入信息的玩家
	 */
	public static void addJoinMessage(EntityPlayer player, String username)
	{
		player.addChatComponentMessage(new ChatComponentText("§e" + username + " 加入了游戏"));
	}
}
