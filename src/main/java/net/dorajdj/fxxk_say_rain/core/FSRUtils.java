package net.dorajdj.fxxk_say_rain.core;

import cpw.mods.fml.common.registry.GameRegistry;
import net.dorajdj.fxxk_say_rain.items.FSRItems;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.oredict.OreDictionary;

public class FSRUtils
{
    public static void registerBlock(boolean isEnable, Block block, String name)
    {
        if (isEnable == true)
        GameRegistry.registerBlock(block, name);
    }
    
    public static void registerBlock(boolean isEnable, Block block, Class<? extends ItemBlock> itemClass, String name)
    {
    	if (isEnable == true)
    	GameRegistry.registerBlock(block, itemClass, name);
    }
    
    public static void registerItem(boolean isEnable, Item item, String name)
    {
        if (isEnable == true)
        GameRegistry.registerItem(item, name);
    }
    
    public static void registerOre(boolean isEnable, String name, Item ore)
    {
    	if (isEnable == true)
    	OreDictionary.registerOre(name, ore);
    }
    
    public static void registerOre(boolean isEnable, String name, Block ore)
    {
    	if (isEnable == true)
    	OreDictionary.registerOre(name, ore);
    }
}
