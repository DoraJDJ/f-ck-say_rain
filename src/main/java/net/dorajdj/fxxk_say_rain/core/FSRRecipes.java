package net.dorajdj.fxxk_say_rain.core;

import net.dorajdj.fxxk_say_rain.blocks.FSRBlocks;
import net.dorajdj.fxxk_say_rain.items.FSRItems;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class FSRRecipes
{
	public static void soulRecipes()
	{
		FSRRecipeUtils.addShapelessRecipes(true, new ItemStack(FSRItems.soulSeed), new Object[] {new ItemStack(Items.wheat_seeds), new ItemStack(Blocks.soul_sand)});
        FSRRecipeUtils.addShapelessRecipes(true, new ItemStack(FSRItems.sayRainSoul), new Object[] {new ItemStack(FSRItems.soulSeed), new ItemStack(Items.dye, 1, 11), new ItemStack(Items.glass_bottle)});
        FSRRecipeUtils.addShapelessRecipes(true, new ItemStack(FSRItems.bakaLSoul), new Object[] {new ItemStack(FSRItems.soulSeed), new ItemStack(Items.dye, 1, 7), new ItemStack(Items.glass_bottle)});
        FSRRecipeUtils.addShapelessRecipes(true, new ItemStack(FSRItems.londingSoul), new Object[] {new ItemStack(FSRItems.soulSeed), new ItemStack(Items.dye, 1, 12), new ItemStack(Items.glass_bottle)});
        FSRRecipeUtils.addShapelessRecipes(FSRConfigHandler.notFFF, new ItemStack(FSRItems.doraJdjSoul), new Object[] {new ItemStack(FSRItems.soulSeed), new ItemStack(Items.dye, 1, 9), new ItemStack(Items.glass_bottle)});
        FSRRecipeUtils.addShapelessRecipes(FSRConfigHandler.notFFF, new ItemStack(FSRItems.puliSoul), new Object[] {new ItemStack(FSRItems.soulSeed), new ItemStack(Items.dye, 1, 5), new ItemStack(Items.glass_bottle)});
        FSRRecipeUtils.addShapelessRecipes(true, new ItemStack(FSRItems.raikoSoul), new Object[] {new ItemStack(FSRItems.soulSeed), new ItemStack(Items.dye, 1, 1), new ItemStack(Items.glass_bottle)});
        FSRRecipeUtils.addShapelessRecipes(true, new ItemStack(FSRItems.himekoSoul), new Object[] {new ItemStack(FSRItems.soulSeed), new ItemStack(Items.dye, 1, 13), new ItemStack(Items.glass_bottle)});
	}
	
	public static void foodRecipes()
	{
		FSRRecipeUtils.addCraftingRecipes(true, new ItemStack(FSRItems.sayRainCookie), new Object[] {"WSW", 'W', Items.wheat, 'S', FSRItems.sayRainSoul});
        FSRRecipeUtils.addShapelessRecipes(true, new ItemStack(FSRItems.sayRainCookie), new Object[] {new ItemStack(Items.cookie), new ItemStack(FSRItems.sayRainSoul)});
        FSRRecipeUtils.addCraftingRecipes(true, new ItemStack(FSRItems.sayRainBread), new Object[] {" S ", "WWW", " S ", 'W', Items.wheat, 'S', FSRItems.sayRainSoul});
        FSRRecipeUtils.addShapelessRecipes(true, new ItemStack(FSRItems.sayRainBread), new Object[] {new ItemStack(Items.bread), new ItemStack(FSRItems.sayRainSoul), new ItemStack(FSRItems.sayRainSoul)});
        FSRRecipeUtils.addShapelessRecipes(true, new ItemStack(FSRItems.sayRainRain), new Object[] {new ItemStack(Items.egg), new ItemStack(FSRItems.sayRainSoul)});
        FSRRecipeUtils.addCraftingRecipes(true, new ItemStack(FSRItems.rainSayRain), new Object[] {"GSG", "SES", "GSG", 'G', Blocks.gold_block, 'S', FSRItems.sayRainSoul, 'E', Items.egg});
	}
	
	public static void skullRecipes()
	{
		FSRRecipeUtils.addShapelessRecipes(true, new ItemStack(FSRBlocks.sayRainBlock), new Object[] {new ItemStack(FSRItems.sayRainSoul), new ItemStack(Blocks.soul_sand)});
        FSRRecipeUtils.addShapelessRecipes(FSRConfigHandler.pumpkinWithSoul, new ItemStack(FSRBlocks.sayRainBlock), new Object[] {new ItemStack(FSRItems.sayRainSoul), new ItemStack(Blocks.pumpkin)});
        
        FSRRecipeUtils.addShapelessRecipes(true, new ItemStack(FSRBlocks.bakaLBlock), new Object[] {new ItemStack(FSRItems.bakaLSoul), new ItemStack(Blocks.soul_sand)});
        FSRRecipeUtils.addShapelessRecipes(FSRConfigHandler.pumpkinWithSoul, new ItemStack(FSRBlocks.bakaLBlock), new Object[] {new ItemStack(FSRItems.bakaLSoul), new ItemStack(Blocks.pumpkin)});
        
        FSRRecipeUtils.addShapelessRecipes(true, new ItemStack(FSRBlocks.londingBlock), new Object[] {new ItemStack(FSRItems.londingSoul), new ItemStack(Blocks.soul_sand)});
        FSRRecipeUtils.addShapelessRecipes(FSRConfigHandler.pumpkinWithSoul, new ItemStack(FSRBlocks.londingBlock), new Object[] {new ItemStack(FSRItems.londingSoul), new ItemStack(Blocks.pumpkin)});
        
        if (FSRConfigHandler.notFFF == true)
        {
            FSRRecipeUtils.addShapelessRecipes(true, new ItemStack(FSRBlocks.doraJdjBlock), new Object[] {new ItemStack(FSRItems.doraJdjSoul), new ItemStack(Blocks.soul_sand)});
            FSRRecipeUtils.addShapelessRecipes(FSRConfigHandler.pumpkinWithSoul, new ItemStack(FSRBlocks.doraJdjBlock), new Object[] {new ItemStack(FSRItems.doraJdjSoul), new ItemStack(Blocks.pumpkin)});
            
            FSRRecipeUtils.addShapelessRecipes(true, new ItemStack(FSRBlocks.puliBlock), new Object[] {new ItemStack(FSRItems.puliSoul), new ItemStack(Blocks.soul_sand)});
            FSRRecipeUtils.addShapelessRecipes(FSRConfigHandler.pumpkinWithSoul, new ItemStack(FSRBlocks.puliBlock), new Object[] {new ItemStack(FSRItems.puliSoul), new ItemStack(Blocks.pumpkin)});
        }
        
        FSRRecipeUtils.addShapelessRecipes(true, new ItemStack(FSRBlocks.raikoBlock), new Object[] {new ItemStack(FSRItems.raikoSoul), new ItemStack(Blocks.soul_sand)});
        FSRRecipeUtils.addShapelessRecipes(FSRConfigHandler.pumpkinWithSoul, new ItemStack(FSRBlocks.raikoBlock), new Object[] {new ItemStack(FSRItems.raikoSoul), new ItemStack(Blocks.pumpkin)});
        
        FSRRecipeUtils.addShapelessRecipes(true, new ItemStack(FSRBlocks.himekoBlock), new Object[] {new ItemStack(FSRItems.himekoSoul), new ItemStack(Blocks.soul_sand)});
        FSRRecipeUtils.addShapelessRecipes(FSRConfigHandler.pumpkinWithSoul, new ItemStack(FSRBlocks.himekoBlock), new Object[] {new ItemStack(FSRItems.himekoSoul), new ItemStack(Blocks.pumpkin)});
	}
	
    public static void yamamotoZnRecipes()
    {
        FSRRecipeUtils.addCraftingRecipes(FSRConfigHandler.enableYamamotoZinc, new ItemStack(FSRItems.yamamotoZn), new Object[] {" S ", "SIS", " S ", 'I', Items.iron_ingot, 'S', FSRItems.sayRainSoul});
        
        FSRRecipeUtils.addCraftingRecipes(FSRConfigHandler.enableYamamotoZinc, new ItemStack(FSRBlocks.yamaZnBlock), new Object[] {" S ", "SIS", " S ", 'I', Blocks.iron_block, 'S', FSRItems.sayRainSoul});
        FSRRecipeUtils.addCraftingRecipes(FSRConfigHandler.enableYamamotoZinc, new ItemStack(FSRBlocks.yamaZnBlock), new Object[] {"ZZZ", "ZZZ", "ZZZ", 'Z', FSRItems.yamamotoZn});
    }
}
