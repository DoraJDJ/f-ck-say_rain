package net.dorajdj.fxxk_say_rain.core;

import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.common.config.Configuration;

public class FSRConfigHandler
{
    /* Custom Crafting Recipes */
    public static boolean pumpkinWithSoul;
    
    /* DoraJDJ & puli Block Setting */
    public static boolean notFFF;
    
    /* Londing Block Setting */
    public static boolean enableLondingBlock;
    
    /* yamamoto Zinc Setting */
    public static boolean enableYamamotoZinc;
    
    /* Rain_say's Egg Setting */
    public static boolean doSomeStrange;
    
    /* Custom Block Message */
    public static boolean enableCustomMsg;
    
    /* Custom Say_rain Block Message */
    public static String customSrMessage1, customSrMessage2, customSrMessage3, customSrMessage4, customSrMessage5;
    
    /* Custom Baka_L Block Message */
    public static String customBMessage1, customBMessage2, customBMessage3, customBMessage4, customBMessage5;
    
    /* Custom Londing Block Message */
    public static String customLMessage1, customLMessage2, customLMessage3, customLMessage4, customLMessage5;
    
    /* Custom DoraJDJ Block Message */
    public static String customJDJMessage1, customJDJMessage2, customJDJMessage3, customJDJMessage4, customJDJMessage5;
    
    /* Custom puli Block Message */
    public static String customPMessage1, customPMessage2, customPMessage3, customPMessage4, customPMessage5;
    
    /* Custom Horikawa_raiko Block Message */
    public static String customHrMessage1, customHrMessage2, customHrMessage3, customHrMessage4, customHrMessage5;
    
    /* Custom HiMeko233 Block Message */
    public static String customHmkMessage1, customHmkMessage2, customHmkMessage3, customHmkMessage4, customHmkMessage5;
    
    private static Configuration FSRConfig;
    
    public static void initConfig(FMLPreInitializationEvent event)
    {
    	FSRConfig = new Configuration(event.getSuggestedConfigurationFile());
    }
    
    public static void getConfig()
    {
    	FSRConfigHandler.FSRConfig.load();
        
        /* Custom Crafting Recipes */
    	FSRConfigHandler.pumpkinWithSoul = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeCrafting, "PumpkinWithSoul", true, "Enable crafting some strange blocks with pumpkins.\n" + "Default: true").getBoolean(true);
        
    	/* DoraJDJ & puli Block Setting */
    	FSRConfigHandler.notFFF = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeReg, "NotFFF", false, "Most people would set this option to false.\n" + "If set to true, don't use fire and any skill in FFF Group!\n" + "Default: false").getBoolean(false);
    	
        /* Londing Block Setting */
        FSRConfigHandler.enableLondingBlock = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlock, "EnableLondingBlock", true, "If set to false, you will show error message when you clicked Londing Block.\n" + "Default: true").getBoolean(true);
        
        /* yamamoto Zinc Setting */
        FSRConfigHandler.enableYamamotoZinc = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeReg, "EnableYamamotoZinc", true, "Do you like yamamoto zinc?\n" + "Default: true").getBoolean(true);
        
        /* Rain_say's Egg Setting */
        FSRConfigHandler.doSomeStrange = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeMisc, "doSomeStrange", true, "Let the rain_say's egg make some strange things...\n" + "Default: true").getBoolean(true);
        
        /* Custom Block Message */
        FSRConfigHandler.enableCustomMsg = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlock, "EnableCustomMessage", false, "Show custom message when player clicked some strange blocks.\n" + "Default: false").getBoolean(false);
        
        /*
        FSRConfigHandler.customTemplateMessage1 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForTemplatee1", "", "Custom messages for Templateee block.").getString();
        FSRConfigHandler.customTemplateMessage2 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForTemplatee2", "").getString();
        FSRConfigHandler.customTemplateMessage3 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForTemplatee3", "").getString();
        FSRConfigHandler.customTemplateMessage4 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForTemplatee4", "").getString();
        FSRConfigHandler.customTemplateMessage5 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForTemplatee5", "").getString();
         */
        
        /* Custom Say_rain Block Message */
        FSRConfigHandler.customSrMessage1 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForSay1", "波卡我爱你哦~", "Custom messages for Say_rain block.").getString();
        FSRConfigHandler.customSrMessage2 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForSay2", "波卡我爱你哦~").getString();
        FSRConfigHandler.customSrMessage3 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForSay3", "波卡我爱你哦~").getString();
        FSRConfigHandler.customSrMessage4 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForSay4", "波卡我爱你哦~").getString();
        FSRConfigHandler.customSrMessage5 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForSay5", "波卡我爱你哦~").getString();
        
        /* Custom Baka_L Block Message */
        FSRConfigHandler.customBMessage1 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForBakaL1", "塞卵，咱们去结婚吧~", "Custom messages for Baka_L block.").getString();
        FSRConfigHandler.customBMessage2 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForBakaL2", "塞卵，咱们去结婚吧~").getString();
        FSRConfigHandler.customBMessage3 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForBakaL3", "塞卵，咱们去结婚吧~").getString();
        FSRConfigHandler.customBMessage4 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForBakaL4", "塞卵，咱们去结婚吧~").getString();
        FSRConfigHandler.customBMessage5 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForBakaL5", "塞卵，咱们去结婚吧~").getString();
        
        /* Custom Londing Block Message */
        FSRConfigHandler.customLMessage1 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForLonding1", "嗯啊...大家觉得...肛我很爽吗？", "Custom messages for Londing block.").getString();
        FSRConfigHandler.customLMessage2 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForLonding2", "嗯啊...大家觉得...肛我很爽吗？").getString();
        FSRConfigHandler.customLMessage3 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForLonding3", "嗯啊...大家觉得...肛我很爽吗？").getString();
        FSRConfigHandler.customLMessage4 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForLonding4", "嗯啊...大家觉得...肛我很爽吗？").getString();
        FSRConfigHandler.customLMessage5 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForLonding5", "嗯啊...大家觉得...肛我很爽吗？").getString();
        
        /* Custom DoraJDJ Block Message */
        FSRConfigHandler.customJDJMessage1 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForJDJ1", "朴莉是我老婆，你们都别想抢！", "Custom messages for DoraJDJ block.").getString();
        FSRConfigHandler.customJDJMessage2 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForJDJ2", "朴莉是我老婆，你们都别想抢！").getString();
        FSRConfigHandler.customJDJMessage3 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForJDJ3", "朴莉是我老婆，你们都别想抢！").getString();
        FSRConfigHandler.customJDJMessage4 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForJDJ4", "朴莉是我老婆，你们都别想抢！").getString();
        FSRConfigHandler.customJDJMessage5 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForJDJ5", "朴莉是我老婆，你们都别想抢！").getString();
        
        /* Custom puli Block Message */
        FSRConfigHandler.customPMessage1 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForPuli1", "JDJ是我老公，你们都别想抢！", "Custom messages for puli block.").getString();
        FSRConfigHandler.customPMessage2 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForPuli2", "JDJ是我老公，你们都别想抢！").getString();
        FSRConfigHandler.customPMessage3 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForPuli3", "JDJ是我老公，你们都别想抢！").getString();
        FSRConfigHandler.customPMessage4 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForPuli4", "JDJ是我老公，你们都别想抢！").getString();
        FSRConfigHandler.customPMessage5 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForPuli5", "JDJ是我老公，你们都别想抢！").getString();
        
        /* Custom Horikawa_raiko Block Message */
        FSRConfigHandler.customHrMessage1 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForRaiko1", "帕琪我的嫁，氩醚你闪开！", "Custom messages for Horikawa_Raiko block.").getString();
        FSRConfigHandler.customHrMessage2 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForRaiko2", "帕琪我的嫁，氩醚你闪开！").getString();
        FSRConfigHandler.customHrMessage3 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForRaiko3", "帕琪我的嫁，氩醚你闪开！").getString();
        FSRConfigHandler.customHrMessage4 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForRaiko4", "帕琪我的嫁，氩醚你闪开！").getString();
        FSRConfigHandler.customHrMessage5 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForRaiko5", "帕琪我的嫁，氩醚你闪开！").getString();
        
        /* Custom HiMeko233 Block Message */
        FSRConfigHandler.customHmkMessage1 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForHimeko1", "对不起氩醚，我和肋骨结婚了...", "Custom messages for HiMeko233 block.").getString();
        FSRConfigHandler.customHmkMessage2 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForHimeko2", "对不起氩醚，我和肋骨结婚了...").getString();
        FSRConfigHandler.customHmkMessage3 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForHimeko3", "对不起氩醚，我和肋骨结婚了...").getString();
        FSRConfigHandler.customHmkMessage4 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForHimeko4", "对不起氩醚，我和肋骨结婚了...").getString();
        FSRConfigHandler.customHmkMessage5 = FSRConfigHandler.FSRConfig.get(FSRRef.configTypeBlockMsg, "CustomMessageForHimeko5", "对不起氩醚，我和肋骨结婚了...").getString();
        
        FSRConfigHandler.FSRConfig.save();
    }
}
