package net.dorajdj.fxxk_say_rain.core;

import java.util.ArrayList;

import cpw.mods.fml.common.registry.GameRegistry;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;

public class FSRRecipeUtils
{
    public static void loadRecipes()
    {
        FSRRecipes.soulRecipes();
        FSRRecipes.foodRecipes();
        FSRRecipes.skullRecipes();
        FSRRecipes.yamamotoZnRecipes();
        
        FSRLogManager.log("Load Recipes Complete");
    }
    
    public static void addCraftingRecipes(Boolean isEnable, ItemStack output, Object... params)
    {
        if (isEnable == true)
            GameRegistry.addRecipe(output, params);
    }
    
    public static void addShapelessRecipes(Boolean isEnable, ItemStack output, Object... params)
    {
        if (isEnable == true)
            GameRegistry.addShapelessRecipe(output, params);
    }
    
    public static void addFurnaceRecipes(Boolean isEnable, Item input, ItemStack output, float xp)
    {
    	if (isEnable == true)
    		GameRegistry.addSmelting(input, output, xp);
    }
    
    public static void addFurnaceRecipes(Boolean isEnable, ItemStack input, ItemStack output, float xp)
    {
    	if (isEnable == true)
    		GameRegistry.addSmelting(input, output, xp);
    }
}
