package net.dorajdj.fxxk_say_rain.itemblocks;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ItemBlockSkull extends ItemBlock
{
	public ItemBlockSkull(Block block)
	{
		super(block);
	}
	
	@Override
	public boolean isValidArmor(ItemStack stack, int armorType, Entity entity)
	{
		return armorType == 0;
	}
}
