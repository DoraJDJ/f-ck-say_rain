package net.dorajdj.fxxk_say_rain;

import net.dorajdj.fxxk_say_rain.blocks.FSRBlocks;
import net.dorajdj.fxxk_say_rain.core.*;
import net.dorajdj.fxxk_say_rain.items.FSRItems;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLInitializationEvent;

@Mod(modid = FSRRef.FSRModid, version = FSRRef.FSRVer, dependencies = "required-after:Forge@[10.12.1.1291,)")

public class Fxxk_say_rain
{
    public static CreativeTabs tabFSR = new CreativeTabs(FSRRef.FSRTab)
    {
    	@Override
    	public Item getTabIconItem()
    	{
    	    return FSRItems.getItemFromBlock(FSRBlocks.sayRainBlock);
    	}
    };
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        FSRLogManager.log("Londing " + FSRRef.FSRModid + "...");
        FSRLogManager.log(FSRRef.FSRModname + " Version: " + FSRRef.FSRVer);
        
        FSRConfigHandler.initConfig(event);
        FSRConfigHandler.getConfig();
        FSRLogManager.log("Load config complete");
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
    	if (FSRConfigHandler.notFFF == true)
    	{
    		FSRLogManager.warn("You enabled DoraJDJ Block and puli Block! Don't use fire and any skill in FFF Group!");
    	}
        FSRItems.registerItems();
        FSRBlocks.registerBlock();
        FSRRecipeUtils.loadRecipes();
    }
}
