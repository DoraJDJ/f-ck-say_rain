package net.dorajdj.fxxk_say_rain.blocks;

import java.util.Random;

import net.dorajdj.fxxk_say_rain.core.FSRConfigHandler;
import net.dorajdj.fxxk_say_rain.core.FSRMsgUtils;
import net.dorajdj.fxxk_say_rain.core.FSRRef;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class SayRainBlock extends Block
{
    public SayRainBlock()
    {
        super(Material.sand);
        this.setStepSound(soundTypeSand);
        this.setHardness(0.5F);
    }
    
    /* Bottom and Top textures */
    /* meta = 0 */
    public static IIcon bottom;
    public static IIcon top;
    /* meta = 1 */
    public static IIcon bottom_1;
    public static IIcon top_1;
    /* meta = 2 */
    public static IIcon bottom_2;
    public static IIcon top_2;
    /* meta = 3 */
    public static IIcon bottom_3;
    public static IIcon top_3;
    /* Side textures */
    public static IIcon sidel;
    public static IIcon sider;
    public static IIcon front;
    public static IIcon back;
    
    public void registerBlockIcons(IIconRegister ir)
    {
        /* Bottom and Top textures */
        /* meta = 0 */
        bottom = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.sayRainBlockID + "_bottom");
        top = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.sayRainBlockID + "_top");
        /* meta = 1 */
        bottom_1 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.sayRainBlockID + "_bottom_1");
        top_1 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.sayRainBlockID + "_top_1");
        /* meta = 2 */
        bottom_2 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.sayRainBlockID + "_bottom_2");
        top_2 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.sayRainBlockID + "_top_2");
        /* meta = 3 */
        bottom_3 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.sayRainBlockID + "_bottom_3");
        top_3 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.sayRainBlockID + "_top_3");
        /* Side textures */
        sidel = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.sayRainBlockID + "_left");
        sider = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.sayRainBlockID + "_right");
        front = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.sayRainBlockID);
        back = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.sayRainBlockID + "_back");
    }
    
    public IIcon getIcon(int side, int meta)
    {
    	if (meta==2 || meta==6) {
    		if (side==0) {
    			return bottom_2;
    		} else if (side==1) {
    			return top_2;
    		} else if (side==2) {
    			return front;
    		} else if (side==3) {
    			return back;
    		} else if (side==4) {
    			return sidel;
    		} else if (side==5) {
    			return sider;
    		}
    		} else if (meta==0 || meta==4) {
    		if (side==0) {
    			return bottom;
    		} else if (side==1) {
    			return top;
    		} else if (side==2) {
    			return back;
    		} else if (side==3) {
    			return front;
    		} else if (side==4) {
    			return sider;
    		} else if (side==5) {
    			return sidel;
    		}
    		} else if (meta==1 || meta==5) {
    		if (side==0) {
    			return bottom_1;
    		} else if (side==1) {
    			return top_1;
    		} else if (side==2) {
    			return sider;
    		} else if (side==3) {
    			return sidel;
    		} else if (side==4) {
    			return front;
    		} else if (side==5) {
    			return back;
    		}
    		} else if (meta==3 || meta==7) {
    		if (side==0) {
    			return bottom_3;
    		} else if (side==1) {
    			return top_3;
    		} else if (side==2) {
    			return sidel;
    		} else if (side==3) {
    			return sider;
    		} else if (side==4) {
    			return back;
    		} else if (side==5) {
    			return front;
    		}
    		}
    		return blockIcon;
    }
    
    public void onBlockPlacedBy(World par1World, int par2, int par3, int par4, EntityLivingBase par5EntityLiving, ItemStack par6ItemStack)
    {
    	int meta = MathHelper.floor_double((double) (par5EntityLiving.rotationYaw * 4.0F / 360.0F) + 2.5D) & 3;
    	par1World.setBlockMetadataWithNotify(par2, par3, par4, meta, 2);
    	
    	// System.out.println("meta = " + meta);
    }
    
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9)
    {
    	if (!world.isRemote)
    	{
    	    if (FSRConfigHandler.enableCustomMsg == false)
    	    {
    	    	int number = new Random().nextInt(20) + 1;
    	        //System.out.println(number);

                if (number == 1)
                {
                	FSRMsgUtils.addChatMessage(player, "Say_rain", "DORA，偷窥狂QAQ");
                }
                else if (number == 2)
                {
                	FSRMsgUtils.addChatMessage(player, "Say_rain", "汪汪汪!唔....汪!");
                }
                else if (number == 3)
                {
                	FSRMsgUtils.addChatMessage(player, "Say_rain", "我是穷人，不要翻辣(*/ω＼*)");
                }
                else if (number == 4)
                {
                	FSRMsgUtils.addChatMessage(player, "Say_rain", "(￣ε(#￣)☆╰╮(￣▽￣///)");
                }
                else if (number == 5)
                {
                	FSRMsgUtils.addChatMessage(player, "Say_rain", "我的违禁品连起来可以绕地球三圈0A0!");
                }
                else if (number == 6)
                {
                	FSRMsgUtils.addChatMessage(player, "Say_rain", "邪恶终究是战胜不了正义的");
                }
                else if (number == 7)
                {
                	FSRMsgUtils.addChatMessage(player, "Say_rain", "我是不会屈服的!");
                }
                else if (number == 8)
                {
                	FSRMsgUtils.addChatMessage(player, "Say_rain", "我要用爱守护我心爱的权限喵");
                }
                else if (number == 9)
                {
                	FSRMsgUtils.addChatMessage(player, "Say_rain", "鶸");
                }
                else if (number == 10)
                {
                	FSRMsgUtils.addChatMessage(player, "Say_rain", "都到这种时候了还看不清局势，66果然要被打倒!");
                }
                else if (number == 11)
                {
                	FSRMsgUtils.addChatMessage(player, "Say_rain", "便太的眼力和正常人就是不一样");
                }
                else if (number == 12)
                {
                	FSRMsgUtils.addChatMessage(player, "Say_rain", "大白兔就是规则!");
                }
                else if (number == 13)
                {
                	FSRMsgUtils.addChatMessage(player, "Say_rain", "靠，大绿兔还下海了");
                }
                else if (number == 14)
                {
                	FSRMsgUtils.addChatMessage(player, "Say_rain", "扶她，认清局势，来加入白兔吧");
                }
                else if (number == 15)
                {
                	FSRMsgUtils.addChatMessage(player, "Say_rain", "猪又不是读条，别随便乱骑");
                }
                else if (number == 16)
                {
                	FSRMsgUtils.addChatMessage(player, "Say_rain", "读条你踏马不是人");
                }
                else if (number == 17)
                {
                	FSRMsgUtils.addChatMessage(player, "Say_rain", "不和你亲，gun");
                }
                else if (number == 18)
                {
                	FSRMsgUtils.addChatMessage(player, "Say_rain", "油库里最萌");
                }
                else if (number == 19)
                {
                	FSRMsgUtils.addChatMessage(player, "Say_rain", "你喊了大绿兔最萌以后就别想来白兔正教了");
                }
                else if (number == 20)
                {
                	FSRMsgUtils.addChatMessage(player, "Say_rain", "干死绿兔子");
                }
                else if (number == 450)
                {
                    /* 因为随机数值最多只能随机到10，所以这个信息并不会正常显示，除非你把源代码给改了 */
                	FSRMsgUtils.addChatMessage(player, "Say_rain", "嗯...啊...波卡...波卡...要去了要去了！！！");
                }
                else
                {
                	FSRMsgUtils.addChatMessage(player, "Say_rain", "DORA，偷窥狂QAQ");
                }
    	    }
    	    else
    	    {
    	    	int numberCM = new Random().nextInt(5) + 1;
    	    	
    	    	if (numberCM == 1)
    	    	{
    	    		FSRMsgUtils.addChatMessage(player, "Say_rain", FSRConfigHandler.customSrMessage1);
    	    	}
    	    	else if (numberCM == 2)
    	    	{
    	    		FSRMsgUtils.addChatMessage(player, "Say_rain", FSRConfigHandler.customSrMessage2);
    	    	}
    	    	else if (numberCM == 3)
    	    	{
    	    		FSRMsgUtils.addChatMessage(player, "Say_rain", FSRConfigHandler.customSrMessage3);
    	    	}
    	    	else if (numberCM == 4)
    	    	{
    	    		FSRMsgUtils.addChatMessage(player, "Say_rain", FSRConfigHandler.customSrMessage4);
    	    	}
    	    	else if (numberCM == 5)
    	    	{
    	    		FSRMsgUtils.addChatMessage(player, "Say_rain", FSRConfigHandler.customSrMessage5);
    	    	}
                else
                {
                	FSRMsgUtils.addChatMessage(player, "Say_rain", "DORA，偷窥狂QAQ");
                }
    	    }
    	}
        
        return true;
    }
}
