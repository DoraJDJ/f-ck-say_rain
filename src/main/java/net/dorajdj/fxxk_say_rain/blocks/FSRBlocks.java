package net.dorajdj.fxxk_say_rain.blocks;

import net.dorajdj.fxxk_say_rain.Fxxk_say_rain;
import net.dorajdj.fxxk_say_rain.core.FSRConfigHandler;
import net.dorajdj.fxxk_say_rain.core.FSRLogManager;
import net.dorajdj.fxxk_say_rain.core.FSRRef;
import net.dorajdj.fxxk_say_rain.core.FSRUtils;
import net.dorajdj.fxxk_say_rain.itemblocks.ItemBlockSkull;
import net.minecraft.block.Block;

public class FSRBlocks
{
    public static Block sayRainBlock;
    public static Block bakaLBlock;
    public static Block londingBlock;
    public static Block doraJdjBlock;
    public static Block puliBlock;
    public static Block raikoBlock;
    public static Block himekoBlock;
    
    public static Block yamaZnBlock;
    
    public static void registerBlock()
    {
        FSRBlocks.sayRainBlock = new SayRainBlock().setBlockTextureName(FSRRef.FSRModid + ":" + FSRRef.sayRainBlockID).setBlockName(FSRRef.sayRainBlockID).setCreativeTab(Fxxk_say_rain.tabFSR);
        FSRBlocks.bakaLBlock = new BakalBlock().setBlockTextureName(FSRRef.FSRModid + ":" + FSRRef.bakaLBlockID).setBlockName(FSRRef.bakaLBlockID).setCreativeTab(Fxxk_say_rain.tabFSR);
        FSRBlocks.londingBlock = new LondingBlock().setBlockTextureName(FSRRef.FSRModid + ":" + FSRRef.londingBlockID).setBlockName(FSRRef.londingBlockID).setCreativeTab(Fxxk_say_rain.tabFSR);
        FSRBlocks.doraJdjBlock = new DoraJdjBlock().setBlockTextureName(FSRRef.FSRModid + ":" + FSRRef.doraJdjBlockID).setBlockName(FSRRef.doraJdjBlockID).setCreativeTab(Fxxk_say_rain.tabFSR);
        FSRBlocks.puliBlock = new PuliBlock().setBlockTextureName(FSRRef.FSRModid + ":" + FSRRef.puliBlockID).setBlockName(FSRRef.puliBlockID).setCreativeTab(Fxxk_say_rain.tabFSR);
        FSRBlocks.raikoBlock = new RaikoBlock().setBlockTextureName(FSRRef.FSRModid + ":" + FSRRef.raikoBlockID).setBlockName(FSRRef.raikoBlockID).setCreativeTab(Fxxk_say_rain.tabFSR);
        FSRBlocks.himekoBlock = new HimekoBlock().setBlockTextureName(FSRRef.FSRModid + ":" + FSRRef.himekoBlockID).setBlockName(FSRRef.himekoBlockID).setCreativeTab(Fxxk_say_rain.tabFSR);
        
        FSRBlocks.yamaZnBlock = new YamaZnBlock().setBlockTextureName(FSRRef.FSRModid + ":" + FSRRef.yamaZnBlockID).setBlockName(FSRRef.yamaZnBlockID).setCreativeTab(Fxxk_say_rain.tabFSR);
        
        FSRUtils.registerBlock(true, FSRBlocks.sayRainBlock, ItemBlockSkull.class, FSRRef.sayRainBlockID);
        FSRUtils.registerBlock(true, FSRBlocks.bakaLBlock, ItemBlockSkull.class, FSRRef.bakaLBlockID);
        FSRUtils.registerBlock(true, FSRBlocks.londingBlock, ItemBlockSkull.class, FSRRef.londingBlockID);
        FSRUtils.registerBlock(FSRConfigHandler.notFFF, FSRBlocks.doraJdjBlock, ItemBlockSkull.class, FSRRef.doraJdjBlockID);
        FSRUtils.registerBlock(FSRConfigHandler.notFFF, FSRBlocks.puliBlock, ItemBlockSkull.class, FSRRef.puliBlockID);
        FSRUtils.registerBlock(true, FSRBlocks.raikoBlock, ItemBlockSkull.class, FSRRef.raikoBlockID);
        FSRUtils.registerBlock(true, FSRBlocks.himekoBlock, ItemBlockSkull.class, FSRRef.himekoBlockID);
        
        FSRUtils.registerBlock(FSRConfigHandler.enableYamamotoZinc, FSRBlocks.yamaZnBlock, FSRRef.yamaZnBlockID);
        FSRUtils.registerOre(FSRConfigHandler.enableYamamotoZinc, "blockZinc", FSRBlocks.yamaZnBlock);
        
        FSRLogManager.log("Register Blocks complete");
    }
}
