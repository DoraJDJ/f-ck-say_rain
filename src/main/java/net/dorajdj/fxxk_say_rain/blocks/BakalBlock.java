package net.dorajdj.fxxk_say_rain.blocks;

import java.util.Random;

import net.dorajdj.fxxk_say_rain.core.FSRConfigHandler;
import net.dorajdj.fxxk_say_rain.core.FSRMsgUtils;
import net.dorajdj.fxxk_say_rain.core.FSRRef;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class BakalBlock extends Block
{
    public BakalBlock()
    {
        super(Material.sand);
        this.setStepSound(soundTypeSand);
        this.setHardness(0.5F);
    }
    
    /* Bottom and Top textures */
    /* meta = 0 */
    public static IIcon bottom;
    public static IIcon top;
    /* meta = 1 */
    public static IIcon bottom_1;
    public static IIcon top_1;
    /* meta = 2 */
    public static IIcon bottom_2;
    public static IIcon top_2;
    /* meta = 3 */
    public static IIcon bottom_3;
    public static IIcon top_3;
    /* Side textures */
    public static IIcon sidel;
    public static IIcon sider;
    public static IIcon front;
    public static IIcon back;
    
    public void registerBlockIcons(IIconRegister ir)
    {
        /* Bottom and Top textures */
        /* meta = 0 */
        bottom = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.bakaLBlockID + "_bottom");
        top = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.bakaLBlockID + "_top");
        /* meta = 1 */
        bottom_1 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.bakaLBlockID + "_bottom_1");
        top_1 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.bakaLBlockID + "_top_1");
        /* meta = 2 */
        bottom_2 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.bakaLBlockID + "_bottom_2");
        top_2 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.bakaLBlockID + "_top_2");
        /* meta = 3 */
        bottom_3 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.bakaLBlockID + "_bottom_3");
        top_3 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.bakaLBlockID + "_top_3");
        /* Side textures */
        sidel = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.bakaLBlockID + "_left");
        sider = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.bakaLBlockID + "_right");
        front = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.bakaLBlockID);
        back = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.bakaLBlockID + "_back");
    }
    
    public IIcon getIcon(int side, int meta)
    {
    	if (meta==2 || meta==6) {
    		if (side==0) {
    			return bottom_2;
    		} else if (side==1) {
    			return top_2;
    		} else if (side==2) {
    			return front;
    		} else if (side==3) {
    			return back;
    		} else if (side==4) {
    			return sidel;
    		} else if (side==5) {
    			return sider;
    		}
    		} else if (meta==0 || meta==4) {
    		if (side==0) {
    			return bottom;
    		} else if (side==1) {
    			return top;
    		} else if (side==2) {
    			return back;
    		} else if (side==3) {
    			return front;
    		} else if (side==4) {
    			return sider;
    		} else if (side==5) {
    			return sidel;
    		}
    		} else if (meta==1 || meta==5) {
    		if (side==0) {
    			return bottom_1;
    		} else if (side==1) {
    			return top_1;
    		} else if (side==2) {
    			return sider;
    		} else if (side==3) {
    			return sidel;
    		} else if (side==4) {
    			return front;
    		} else if (side==5) {
    			return back;
    		}
    		} else if (meta==3 || meta==7) {
    		if (side==0) {
    			return bottom_3;
    		} else if (side==1) {
    			return top_3;
    		} else if (side==2) {
    			return sidel;
    		} else if (side==3) {
    			return sider;
    		} else if (side==4) {
    			return back;
    		} else if (side==5) {
    			return front;
    		}
    		}
    		return blockIcon;
    }
    
    public void onBlockPlacedBy(World par1World, int par2, int par3, int par4, EntityLivingBase par5EntityLiving, ItemStack par6ItemStack)
    {
    	int meta = MathHelper.floor_double((double) (par5EntityLiving.rotationYaw * 4.0F / 360.0F) + 2.5D) & 3;
    	par1World.setBlockMetadataWithNotify(par2, par3, par4, meta, 2);
    	
    	// System.out.println("meta = " + meta);
    }
    
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9)
    {
    	if (!world.isRemote)
    	{
    	    if (FSRConfigHandler.enableCustomMsg == false)
    	    {
    	    	int number = new Random().nextInt(23) + 1;
    	        //System.out.println(number);

                if (number == 1)
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "贱人小婊砸塞卵卵");
                }
                else if (number == 2)
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "看我凌辱你们");
                }
                else if (number == 3)
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "博物馆有个妓术方面的问题所以暂时不开，233");
                }
                else if (number == 4)
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "权限狗塞卵，这就禁了塞卵的TP");
                }
                else if (number == 5)
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "我现在没做人，我现在是做啵卡");
                }
                else if (number == 6)
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "鸡蛋卷你要跟塞卵在一起么233那你也来一小时吧~");
                }
                else if (number == 7)
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "权限狗塞卵终究还是没法战胜更大的权限狗233");
                }
                else if (number == 8)
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "今天就叫saydediyici好了");
                }
                else if (number == 9)
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "于是今天就不插♂了");
                }
                else if (number == 10)
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "tm一定是xoe上了我的号给我换过床了");
                }
                else if (number == 11)
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "我tm一定不知不觉在别人床上睡过了！QAQ");
                }
                else if (number == 12)
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "小伙子，蜀黍不约");
                }
                else if (number == 13)
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "你是要种子还是种♂子还是种♂♂♂子");
                }
                else if (number == 14)
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "考拉我们来赛马");
                }
                else if (number == 15)
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "人一有权利就会贱");
                }
                else if (number == 16)
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "汪汪汪！");
                }
                else if (number == 17)
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "劳资不做人啦！！！！劳资不做人啦！！！！不做人啦！！！！");
                }
                else if (number == 18)
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "变态女仆终于放弃了自己的羞耻心，变成了彻彻底底的变态");
                }
                else if (number == 19)
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "好！你赢了！我傻逼！");
                }
                else if (number == 20)
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "一入⑨服深似海，此生无悔草66");
                }
                else if (number == 21)
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "say这个死变态越来越变态了");
                }
                else if (number == 22)
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "我至今记忆犹新P_P");
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "那是一个风和日丽的下午");
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "我在教堂里思考怎么搭最后的晚餐");
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "然后一道圣光！");
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "我感受到了神的指示！");
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "tm有个变态要降临到这个世界上了！");
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "然后say就来了(:3JZ");
                }
                else if (number == 23)
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "感谢大家多年的支持，打完这场仗我就和塞卵去结婚");
                }
                else if (number == 450)
                {
                    /* 因为随机数值最多只能随机到10，所以这个信息并不会正常显示，除非你把源代码给改了 */
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "噢噢噢噢噢！！！我要把我的XX身寸到塞卵的XX里！！！");
                }
                else
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "贱人小婊砸塞卵卵");
                }
    	    }
    	    else
    	    {
    	    	int numberCM = new Random().nextInt(5) + 1;
    	    	
    	    	if (numberCM == 1)
    	    	{
    	    		FSRMsgUtils.addChatMessage(player, "Baka_L", FSRConfigHandler.customBMessage1);
    	    	}
    	    	else if (numberCM == 2)
    	    	{
    	    		FSRMsgUtils.addChatMessage(player, "Baka_L", FSRConfigHandler.customBMessage2);
    	    	}
    	    	else if (numberCM == 3)
    	    	{
    	    		FSRMsgUtils.addChatMessage(player, "Baka_L", FSRConfigHandler.customBMessage3);
    	    	}
    	    	else if (numberCM == 4)
    	    	{
    	    		FSRMsgUtils.addChatMessage(player, "Baka_L", FSRConfigHandler.customBMessage4);
    	    	}
    	    	else if (numberCM == 5)
    	    	{
    	    		FSRMsgUtils.addChatMessage(player, "Baka_L", FSRConfigHandler.customBMessage5);
    	    	}
                else
                {
                	FSRMsgUtils.addChatMessage(player, "Baka_L", "贱人小婊砸塞卵卵");
                }
    	    }
    	}
        
        return true;
    }
}
