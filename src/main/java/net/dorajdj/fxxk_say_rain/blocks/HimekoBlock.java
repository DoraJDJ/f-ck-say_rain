package net.dorajdj.fxxk_say_rain.blocks;

import java.util.Random;

import net.dorajdj.fxxk_say_rain.core.FSRConfigHandler;
import net.dorajdj.fxxk_say_rain.core.FSRMsgUtils;
import net.dorajdj.fxxk_say_rain.core.FSRRef;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class HimekoBlock extends Block
{
    public HimekoBlock()
    {
        super(Material.sand);
        this.setStepSound(soundTypeSand);
        this.setHardness(0.5F);
    }
    
    /* Bottom and Top textures */
    /* meta = 0 */
    public static IIcon bottom;
    public static IIcon top;
    /* meta = 1 */
    public static IIcon bottom_1;
    public static IIcon top_1;
    /* meta = 2 */
    public static IIcon bottom_2;
    public static IIcon top_2;
    /* meta = 3 */
    public static IIcon bottom_3;
    public static IIcon top_3;
    /* Side textures */
    public static IIcon sidel;
    public static IIcon sider;
    public static IIcon front;
    public static IIcon back;
    
    public void registerBlockIcons(IIconRegister ir)
    {
        /* Bottom and Top textures */
        /* meta = 0 */
        bottom = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.himekoBlockID + "_bottom");
        top = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.himekoBlockID + "_top");
        /* meta = 1 */
        bottom_1 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.himekoBlockID + "_bottom_1");
        top_1 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.himekoBlockID + "_top_1");
        /* meta = 2 */
        bottom_2 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.himekoBlockID + "_bottom_2");
        top_2 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.himekoBlockID + "_top_2");
        /* meta = 3 */
        bottom_3 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.himekoBlockID + "_bottom_3");
        top_3 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.himekoBlockID + "_top_3");
        /* Side textures */
        sidel = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.himekoBlockID + "_left");
        sider = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.himekoBlockID + "_right");
        front = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.himekoBlockID);
        back = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.himekoBlockID + "_back");
    }
    
    public IIcon getIcon(int side, int meta)
    {
    	if (meta==2 || meta==6) {
    		if (side==0) {
    			return bottom_2;
    		} else if (side==1) {
    			return top_2;
    		} else if (side==2) {
    			return front;
    		} else if (side==3) {
    			return back;
    		} else if (side==4) {
    			return sidel;
    		} else if (side==5) {
    			return sider;
    		}
    		} else if (meta==0 || meta==4) {
    		if (side==0) {
    			return bottom;
    		} else if (side==1) {
    			return top;
    		} else if (side==2) {
    			return back;
    		} else if (side==3) {
    			return front;
    		} else if (side==4) {
    			return sider;
    		} else if (side==5) {
    			return sidel;
    		}
    		} else if (meta==1 || meta==5) {
    		if (side==0) {
    			return bottom_1;
    		} else if (side==1) {
    			return top_1;
    		} else if (side==2) {
    			return sider;
    		} else if (side==3) {
    			return sidel;
    		} else if (side==4) {
    			return front;
    		} else if (side==5) {
    			return back;
    		}
    		} else if (meta==3 || meta==7) {
    		if (side==0) {
    			return bottom_3;
    		} else if (side==1) {
    			return top_3;
    		} else if (side==2) {
    			return sidel;
    		} else if (side==3) {
    			return sider;
    		} else if (side==4) {
    			return back;
    		} else if (side==5) {
    			return front;
    		}
    		}
    		return blockIcon;
    }
    
    public void onBlockPlacedBy(World par1World, int par2, int par3, int par4, EntityLivingBase par5EntityLiving, ItemStack par6ItemStack)
    {
    	int meta = MathHelper.floor_double((double) (par5EntityLiving.rotationYaw * 4.0F / 360.0F) + 2.5D) & 3;
    	par1World.setBlockMetadataWithNotify(par2, par3, par4, meta, 2);
    	
    	// System.out.println("meta = " + meta);
    }
    
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9)
    {
    	if (!world.isRemote)
    	{
    	    if (FSRConfigHandler.enableCustomMsg == false)
    	    {
    	    	int number = new Random().nextInt(15) + 1;
    	        //System.out.println(number);

                if (number == 1)
                {
                	FSRMsgUtils.addChatMessage(player, "HiMeko233", "呱！");
                }
                else if (number == 2)
                {
                	FSRMsgUtils.addChatMessage(player, "HiMeko233", "PAP");
                }
                else if (number == 3)
                {
                	FSRMsgUtils.addChatMessage(player, "HiMeko233", "我也想养驴PAP然后放马铃薯");
                }
                else if (number == 4)
                {
                	FSRMsgUtils.addChatMessage(player, "HiMeko233", "拉屎不吸收什么鬼23333");
                }
                else if (number == 5)
                {
                	FSRMsgUtils.addChatMessage(player, "HiMeko233", "雪人很萌的你们不要黑他PAP");
                }
                else if (number == 6)
                {
                	FSRMsgUtils.addChatMessage(player, "HiMeko233", "走 咱们去拿TNT");
                }
                else if (number == 7)
                {
                	FSRMsgUtils.addChatMessage(player, "HiMeko233", "对哦 山本本比萝莉还矮233");
                }
                else if (number == 8)
                {
                	FSRMsgUtils.addChatMessage(player, "HiMeko233", "快住手PAP受姬是我其中一个ID啊2333");
                }
                else if (number == 9)
                {
                	FSRMsgUtils.addChatMessage(player, "HiMeko233", "玩♂尻本");
                }
                else if (number == 10)
                {
                	FSRMsgUtils.addChatMessage(player, "HiMeko233", "请叫我本子收藏家！");
                }
                else if (number == 11)
                {
                	FSRMsgUtils.addChatMessage(player, "HiMeko233", "孕→山本←孕");
                }
                else if (number == 12)
                {
                	FSRMsgUtils.addChatMessage(player, "HiMeko233", "这种时候只要微笑就好了:)");
                }
                else if (number == 13)
                {
                	FSRMsgUtils.addChatMessage(player, "HiMeko233", "本本我要零花钱");
                }
                else if (number == 14)
                {
                	FSRMsgUtils.addChatMessage(player, "HiMeko233", "晚饭吃火锅嘟噜噜");
                }
                else if (number == 15)
                {
                	FSRMsgUtils.addChatMessage(player, "HiMeko233", "马铃薯轰炸姬！");
                }
                else if (number == 450)
                {
                    /* 因为随机数值最多只能随机到10，所以这个信息并不会正常显示，除非你把源代码给改了 */
                	FSRMsgUtils.addChatMessage(player, "HiMeko233", "诸君！！不好了！！啪居然不是futa！！");
                }
                else
                {
                	FSRMsgUtils.addChatMessage(player, "HiMeko233", "呱！");
                }
    	    }
    	    else
    	    {
    	    	int numberCM = new Random().nextInt(5) + 1;
    	    	
    	    	if (numberCM == 1)
    	    	{
    	    		FSRMsgUtils.addChatMessage(player, "HiMeko233", FSRConfigHandler.customHmkMessage1);
    	    	}
    	    	else if (numberCM == 2)
    	    	{
    	    		FSRMsgUtils.addChatMessage(player, "HiMeko233", FSRConfigHandler.customHmkMessage2);
    	    	}
    	    	else if (numberCM == 3)
    	    	{
    	    		FSRMsgUtils.addChatMessage(player, "HiMeko233", FSRConfigHandler.customHmkMessage3);
    	    	}
    	    	else if (numberCM == 4)
    	    	{
    	    		FSRMsgUtils.addChatMessage(player, "HiMeko233", FSRConfigHandler.customHmkMessage4);
    	    	}
    	    	else if (numberCM == 5)
    	    	{
    	    		FSRMsgUtils.addChatMessage(player, "HiMeko233", FSRConfigHandler.customHmkMessage5);
    	    	}
                else
                {
                	FSRMsgUtils.addChatMessage(player, "HiMeko233", "呱！");
                }
    	    }
    	}
        
        return true;
    }
}
