package net.dorajdj.fxxk_say_rain.blocks;

import java.util.Random;

import net.dorajdj.fxxk_say_rain.core.FSRMsgUtils;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.Explosion;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class YamaZnBlock extends Block
{
	public YamaZnBlock()
	{
		super(Material.iron);
		this.setStepSound(soundTypeAnvil);
		this.setHardness(5.0F);
		this.setResistance(10.0F);
	}
	
	@Override
	public boolean isBeaconBase(IBlockAccess worldObj, int x, int y, int z, int beaconX, int beaconY, int beaconZ)
	{
        return true;
    }
	
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9)
    {
		if (!world.isRemote)
    	{
			int number = new Random().nextInt(18) + 1;
			
			if (number == 1)
            {
				FSRMsgUtils.addChatMessage(player, "yamamotoaaaa", "ypaaaaaaaaaaaaa");
            }
            else if (number == 2)
            {
            	FSRMsgUtils.addChatMessage(player, "yamamotoaaaa", "我发现puli和鸡蛋卷是两个失恋的人强行凑在一起吧233");
            }
            else if (number == 3)
            {
            	FSRMsgUtils.addChatMessage(player, "yamamotoaaaa", "你个挂逼，来sala");
            }
            else if (number == 4)
            {
            	FSRMsgUtils.addChatMessage(player, "yamamotoaaaa", "艹，你干嘛，是不是欠打");
            }
            else if (number == 5)
            {
            	FSRMsgUtils.addChatMessage(player, "yamamotoaaaa", "艹，你们能不能肛正面");
            }
            else if (number == 6)
            {
            	FSRMsgUtils.addChatMessage(player, "yamamotoaaaa", "无耻gouB233");
            }
            else if (number == 7)
            {
            	FSRMsgUtils.addChatMessage(player, "yamamotoaaaa", "太阳万岁");
            }
            else if (number == 8)
            {
            	FSRMsgUtils.addChatMessage(player, "yamamotoaaaa", "塞卵真不要脸233");
            }
            else if (number == 9)
            {
            	FSRMsgUtils.addChatMessage(player, "yamamotoaaaa", "鸡蛋卷你不要帕琪塞卵了吗，真是丑陋");
            }
            else if (number == 10)
            {
            	FSRMsgUtils.addChatMessage(player, "yamamotoaaaa", "权限doge");
            }
            else if (number == 11)
            {
            	FSRMsgUtils.addChatMessage(player, "yamamotoaaaa", "来咬我啊");
            }
            else if (number == 12)
            {
            	FSRMsgUtils.addChatMessage(player, "yamamotoaaaa", "不服sala");
            }
            else if (number == 13)
            {
            	FSRMsgUtils.addChatMessage(player, "yamamotoaaaa", "菜鸡");
            }
            else if (number == 14)
            {
            	FSRMsgUtils.addChatMessage(player, "yamamotoaaaa", "装了B就跑");
            }
            else if (number == 15)
            {
            	FSRMsgUtils.addChatMessage(player, "yamamotoaaaa", "你们可以生几个小鸡蛋卷");
            }
            else if (number == 16)
            {
            	FSRMsgUtils.addChatMessage(player, "yamamotoaaaa", "菜鸡鸡蛋卷，菜鸡puli");
            }
            else if (number == 17)
            {
            	FSRMsgUtils.addChatMessage(player, "yamamotoaaaa", "双人组没有一个看着背后的话就没意义");
            }
            else if (number == 18)
            {
            	FSRMsgUtils.addChatMessage(player, "yamamotoaaaa", "双人组的精髓就在于不怕被人偷袭，但是这点你们没做到");
            }
            else
            {
            	FSRMsgUtils.addChatMessage(player, "yamamotoaaaa", "ypaaaaaaaaaaaaa");
            }
			
			player.addPotionEffect(new PotionEffect(Potion.poison.id, 200, 0));
    	}
		
		return true;
	}
}
