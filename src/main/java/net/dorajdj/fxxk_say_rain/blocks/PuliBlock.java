package net.dorajdj.fxxk_say_rain.blocks;

import java.util.Random;

import net.dorajdj.fxxk_say_rain.core.FSRConfigHandler;
import net.dorajdj.fxxk_say_rain.core.FSRMsgUtils;
import net.dorajdj.fxxk_say_rain.core.FSRRef;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class PuliBlock extends Block
{
    public PuliBlock()
    {
        super(Material.sand);
        this.setStepSound(soundTypeSand);
        this.setHardness(0.5F);
    }
    
    /* Bottom and Top textures */
    /* meta = 0 */
    public static IIcon bottom;
    public static IIcon top;
    /* meta = 1 */
    public static IIcon bottom_1;
    public static IIcon top_1;
    /* meta = 2 */
    public static IIcon bottom_2;
    public static IIcon top_2;
    /* meta = 3 */
    public static IIcon bottom_3;
    public static IIcon top_3;
    /* Side textures */
    public static IIcon sidel;
    public static IIcon sider;
    public static IIcon front;
    public static IIcon back;
    
    public void registerBlockIcons(IIconRegister ir)
    {
        /* Bottom and Top textures */
        /* meta = 0 */
        bottom = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.puliBlockID + "_bottom");
        top = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.puliBlockID + "_top");
        /* meta = 1 */
        bottom_1 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.puliBlockID + "_bottom_1");
        top_1 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.puliBlockID + "_top_1");
        /* meta = 2 */
        bottom_2 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.puliBlockID + "_bottom_2");
        top_2 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.puliBlockID + "_top_2");
        /* meta = 3 */
        bottom_3 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.puliBlockID + "_bottom_3");
        top_3 = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.puliBlockID + "_top_3");
        /* Side textures */
        sidel = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.puliBlockID + "_left");
        sider = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.puliBlockID + "_right");
        front = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.puliBlockID);
        back = ir.registerIcon(FSRRef.FSRModid + ":" + FSRRef.puliBlockID + "_back");
    }
    
    public IIcon getIcon(int side, int meta)
    {
    	if (meta==2 || meta==6) {
    		if (side==0) {
    			return bottom_2;
    		} else if (side==1) {
    			return top_2;
    		} else if (side==2) {
    			return front;
    		} else if (side==3) {
    			return back;
    		} else if (side==4) {
    			return sidel;
    		} else if (side==5) {
    			return sider;
    		}
    		} else if (meta==0 || meta==4) {
    		if (side==0) {
    			return bottom;
    		} else if (side==1) {
    			return top;
    		} else if (side==2) {
    			return back;
    		} else if (side==3) {
    			return front;
    		} else if (side==4) {
    			return sider;
    		} else if (side==5) {
    			return sidel;
    		}
    		} else if (meta==1 || meta==5) {
    		if (side==0) {
    			return bottom_1;
    		} else if (side==1) {
    			return top_1;
    		} else if (side==2) {
    			return sider;
    		} else if (side==3) {
    			return sidel;
    		} else if (side==4) {
    			return front;
    		} else if (side==5) {
    			return back;
    		}
    		} else if (meta==3 || meta==7) {
    		if (side==0) {
    			return bottom_3;
    		} else if (side==1) {
    			return top_3;
    		} else if (side==2) {
    			return sidel;
    		} else if (side==3) {
    			return sider;
    		} else if (side==4) {
    			return back;
    		} else if (side==5) {
    			return front;
    		}
    		}
    		return blockIcon;
    }
    
    public void onBlockPlacedBy(World par1World, int par2, int par3, int par4, EntityLivingBase par5EntityLiving, ItemStack par6ItemStack)
    {
    	int meta = MathHelper.floor_double((double) (par5EntityLiving.rotationYaw * 4.0F / 360.0F) + 2.5D) & 3;
    	par1World.setBlockMetadataWithNotify(par2, par3, par4, meta, 2);
    	
    	// System.out.println("meta = " + meta);
    }
    
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9)
    {
    	if (!world.isRemote)
    	{
    	    if (FSRConfigHandler.enableCustomMsg == false)
    	    {
    	    	int number = new Random().nextInt(10) + 1;
    	        //System.out.println(number);

                if (number == 1)
                {
                	FSRMsgUtils.addChatMessage(player, "puli", "niconiconi~");
                }
                else if (number == 2)
                {
                	FSRMsgUtils.addChatMessage(player, "puli", "JDJ你胸太小2333");
                }
                else if (number == 3)
                {
                	FSRMsgUtils.addChatMessage(player, "puli", "JDJ我爱你_(:зゝ∠)_");
                }
                else if (number == 4)
                {
                	FSRMsgUtils.addChatMessage(player, "puli", "读条有欧派吗2333");
                }
                else if (number == 5)
                {
                	FSRMsgUtils.addChatMessage(player, "puli", "已入JDJ后宫get√");
                }
                else if (number == 6)
                {
                	FSRMsgUtils.addChatMessage(player, "puli", "反正我是JDJ的了_(:зゝ∠)_");
                }
                else if (number == 7)
                {
                	FSRMsgUtils.addChatMessage(player, "puli", "想跟我比胸吗？我是不会输的！");
                }
                else if (number == 8)
                {
                	FSRMsgUtils.addChatMessage(player, "puli", "最喜欢JDJ了_(:зゝ∠)_");
                }
                else if (number == 9)
                {
                	FSRMsgUtils.addChatMessage(player, "puli", "⑨服里只有JDJ才才买得起朴莉");
                }
                else if (number == 10)
                {
                	FSRMsgUtils.addChatMessage(player, "puli", "双人组的精髓在于互相给予快感2333");
                }
                else if (number == 450)
                {
                    /* 因为随机数值最多只能随机到10，所以这个信息并不会正常显示，除非你把源代码给改了 */
                	FSRMsgUtils.addChatMessage(player, "puli", "啊♂");
                	FSRMsgUtils.addChatMessage(player, "DoraJDJ", "嗯♂");
                }
                else
                {
                	FSRMsgUtils.addChatMessage(player, "puli", "niconiconi~");
                }
    	    }
    	    else
    	    {
    	    	int numberCM = new Random().nextInt(5) + 1;
    	    	
    	    	if (numberCM == 1)
    	    	{
    	    		FSRMsgUtils.addChatMessage(player, "puli", FSRConfigHandler.customPMessage1);
    	    	}
    	    	else if (numberCM == 2)
    	    	{
    	    		FSRMsgUtils.addChatMessage(player, "puli", FSRConfigHandler.customPMessage2);
    	    	}
    	    	else if (numberCM == 3)
    	    	{
    	    		FSRMsgUtils.addChatMessage(player, "puli", FSRConfigHandler.customPMessage3);
    	    	}
    	    	else if (numberCM == 4)
    	    	{
    	    		FSRMsgUtils.addChatMessage(player, "puli", FSRConfigHandler.customPMessage4);
    	    	}
    	    	else if (numberCM == 5)
    	    	{
    	    		FSRMsgUtils.addChatMessage(player, "puli", FSRConfigHandler.customPMessage5);
    	    	}
                else
                {
                	FSRMsgUtils.addChatMessage(player, "puli", "niconiconi~");
                }
    	    }
    	}
        
        return true;
    }
}
